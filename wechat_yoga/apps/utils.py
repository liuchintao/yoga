from rest_framework.pagination import PageNumberPagination


def wrap_data(data, obj, method='GET', success=True):
    result = {'data': data}
    if method == 'POST':
        code = 100
        msg = 'Insert '
    elif method == 'DELETE':
        code = 200
        msg = 'Delete '
    elif method == 'PUT':
        code = 300
        msg = 'Update '
    elif method == 'GET':
        code = 400
        msg = 'Select '
    msg = msg + str(obj)
    if not success:
        code += 10
        msg = msg + ' failed.'
    else:
        msg = msg + ' success.'
    result['msg'] = msg
    result['code'] = code
    result['success'] = success
    return result


class YogaPageNumberPagination(PageNumberPagination):
    page_size = 1
    max_page_size = 20
    page_size_query_param = 'size'
    page_query_param = 'page'


def polish_yoga_class(class_data):
    number_2_cn = ['一', '二', '三', '四', '五', '六', '日']
    import datetime
    data = []
    now = datetime.datetime.now()
    for i in range(7):
        d = now + datetime.timedelta(i)
        data.append({
            'date': str(d.date()),
            'weekday': '星期'+number_2_cn[d.isoweekday() - 1],
            'yogas': []
        })
    for yoga in class_data:
        d = datetime.datetime.strptime(str(yoga.startTime), '%Y-%m-%d %H:%M:%S')
        sub_days = now.date().__sub__(d.date()).days * (-1)
        if sub_days > 6 or sub_days < 0:
            continue
        yoga_class = dict()
        yoga_class['startTime'] = yoga.startTime.strftime('%H:%M:%S')
        yoga_class['endTime'] = yoga.endTime.strftime('%H:%M:%S')
        yoga_class['place'] = yoga.placeNum.name
        yoga_class['placeId'] = yoga.placeNum.placeNum
        yoga_class['address'] = yoga.placeNum.address
        yoga_class['teacher'] = yoga.teacherNum.name
        yoga_class['teacherId'] = yoga.teacherNum.teacherNum
        yoga_class['name'] = yoga.lessonNum.name
        yoga_class['difficulty'] = yoga.lessonNum.difficulty
        yoga_class['classNum'] = yoga.classNum
        yoga_class['total'] = yoga.total
        yoga_class['left'] = yoga.left
        data[sub_days]['yogas'].append(yoga_class)
    return data

def md5_encoding(*keys):
    s = ''.join([str(key) for key in keys])
    import hashlib
    hl = hashlib.md5()
    hl.update(s.encode(encoding='utf-8'))
    return hl.hexdigest()