from django.db import models
from django.conf import settings

import os
USER_MEDIA_ROOT = settings.MEDIA_ROOT + os.path.sep + 'user'
USER_AVATAR_ROOT = USER_MEDIA_ROOT + os.path.sep + 'imgs'

from apps.class_model.models import YogaClass
# Create your models here.


class User(models.Model):
    userNum = models.AutoField(primary_key=True)
    name = models.CharField(max_length=16)
    gender = models.CharField(max_length=8)
    age = models.IntegerField()
    phone = models.CharField(max_length=11, unique=True)
    signUpTime = models.DateField(auto_now=True)
    avatar = models.ImageField(upload_to=USER_AVATAR_ROOT, default=None)
    wc_token = models.CharField(max_length=256, null=True)
    password = models.CharField(max_length=16, default='yoga123')
    token = models.CharField(max_length=256, default=None)

    def __str__(self):
        return 'UserNumber: {}, UserName: {}, Gender: {}, \
            Age: {}, Phone: {}, SignUpTime: {}'\
                .format(self.userNum, self.name, self.gender, \
                    self.age, self.phone, self.signUpTime)


class SubScription(models.Model):
    userNum = models.ForeignKey('User', on_delete=models.CASCADE, related_name='subss_user')
    classNum = models.ForeignKey(
        'class_model.YogaClass', on_delete=models.CASCADE, related_name='yoga_class')
    subscribeTime = models.DateTimeField()
    overred = models.BooleanField()
    classCmtLv = models.IntegerField(default=-1)
    teacherCmtLv = models.IntegerField(default=-1)
    classCmt = models.CharField(max_length=512, default="用户未评价，默认好评！")
    teacherCmt = models.CharField(max_length=512, default="用户未评价，默认好评！")
    commented = models.BooleanField(default=False)

    def __str__(self):
        return 'UserNumber: {}, ClassNumber: {}, SrbscribeTime: {}, \
            ClassComment:{}, TeacherComment: {}, ClassCommentLevel: {}, TeacherCommentLevel: {}'\
                .format(self.userNum, self.classNum, self.subscribeTime, \
                    self.classCmt, self.teacherCmt, self.classCmtLv, self.teacherCmtLv)
