def process_sub_fmt(subs):
    import datetime
    now_date = datetime.datetime.now()
    now = int(now_date.strftime('%Y%m%d%H%M%S'))
    subscriptions = []
    for sub in subs:
      start = int(sub.classNum.startTime.strftime('%Y%m%d%H%M%S'))
      end = int(sub.classNum.endTime.strftime('%Y%m%d%H%M%S'))
      sub_info = {}
      print('now:', now, 'start:', start, 'end', end)
      if now >= start and now <= end:
        sub_info['state'] = '已开始'
        sub_info['stateIcon'] = 'el-icon-success'
      elif now < start:
        sub_info['state'] = '未开始'
        sub_info['stateIcon'] = 'el-icon-alarm-clock'
      else:
        continue
      sub_info['date'] = sub.classNum.startTime.strftime('%Y-%m-%d')
      sub_info['startTime'] = sub.classNum.startTime.strftime('%H:%M:%S')
      sub_info['endTime'] = sub.classNum.endTime.strftime('%H:%M:%S')
      sub_info['teacher'] = sub.classNum.teacherNum.name
      sub_info['address'] = sub.classNum.placeNum.address
      sub_info['place'] = sub.classNum.placeNum.name
      sub_info['lesson'] = sub.classNum.lessonNum.name
      sub_info['subId'] = sub.id
      subscriptions.append(sub_info)
    return subscriptions