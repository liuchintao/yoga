from rest_framework import serializers

from apps.user_model.models import User, SubScription


class UserSerializer(serializers.ModelSerializer):
    avatar = serializers.ImageField(max_length=None, allow_empty_file=False, use_url=True)
    class Meta:
        model = User
        fields = ('userNum', 'name', 'gender', 'age', 'phone', 'signUpTime', 'avatar', 'wc_token', 'token')


class SubScriptionSerializer(serializers.ModelSerializer):
    user_number = serializers.IntegerField(
        source='userNum.userNum', read_only=True)
    user_name = serializers.CharField(source='userNum.name', read_only=True)
    class_number = serializers.IntegerField(
        source='classNum.classNum', read_only=True)
    class_name = serializers.CharField(
        source='classNum.lessonNum.name', read_only=True)
    class_start = serializers.DateTimeField(source='classNum.startTime', read_only=True)
    class_end = serializers.DateTimeField(source='classNum.endTime', read_only=True)
    place_number = serializers.IntegerField(
        source='classNum.placeNum.placeNum', read_only=True)
    place_name = serializers.CharField(
        source='classNum.placeNum.name', read_only=True)
    address = serializers.CharField(source='classNum.placeNum.address', read_only=True)
    teacher_number= serializers.IntegerField(source='classNum.teacherNum.teacherNum', read_only=True)
    teacher_name= serializers.CharField(source='classNum.teacherNum.name', read_only=True)

    class Meta:
        model = SubScription
        fields = ('id', 'user_number', 'user_name', 'class_number', 'class_name', 
        'subscribeTime', 'overred', 'classCmtLv', 'teacherCmtLv', 'classCmt', 'teacherCmt', 
        'commented', 'class_start', 'class_end', 'place_number', 'place_name', 'address',
        'teacher_number', 'teacher_name')
