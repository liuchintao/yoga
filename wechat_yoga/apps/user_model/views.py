from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.conf import settings
from rest_framework import parsers
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView

from apps.utils import wrap_data, YogaPageNumberPagination, md5_encoding
from apps.user_model.utils import process_sub_fmt
from apps.user_model.models import User, SubScription
from apps.user_model.serializers import UserSerializer, SubScriptionSerializer
from apps.class_model.models import YogaClass
# Create your views here.

######################
# User related views #
######################
@csrf_exempt
def user_detail(request, token):
    try:
        user = User.objects.get(token=token)
    except User.DoesNotExist:
        data = wrap_data(None, 'user ' + str(id), request.method, False)
    if request.method == 'GET':
        serializer = UserSerializer(user)
        subs = user.subss_user.all()
        subsData = process_sub_fmt(subs)
        data = wrap_data({'user_info': serializer.data, 'sub_info': subsData}, 'user ' + str(id))
    return JsonResponse(data, json_dumps_params={'ensure_ascii': False})


@csrf_exempt
def user_check(request):
    if request.method != 'POST':
        data = wrap_data({}, 'user', request.method, False)
    else:
        name = request.POST.get('name')
        phone = request.POST.get('phone')
        try:
            user = User.objects.get(phone=phone)
        except User.DoesNotExist:
            data = wrap_data({}, 'user', request.method, False)
        if user is not None and user.name == name:
            data = wrap_data({'token': user.token}, 'user', request.method, True)
        else:
            data = wrap_data({}, 'user', request.method, False)
    return JsonResponse(data, json_dumps_params={'ensure_ascii': False})


class UsersView(APIView):
    parser_classes = (parsers.FormParser, parsers.MultiPartParser,
                      parsers.JSONParser, parsers.FileUploadParser,)

    def get(self, request, *args, **kwargs):
        page = YogaPageNumberPagination()
        users = User.objects.get_queryset()
        page_users = page.paginate_queryset(queryset=users, request=request, view=self)
        serializer = UserSerializer(page_users, many=True)
        data = wrap_data(serializer.data, 'user list', request.method, True)
        pageNum=request.GET.get('page', default=1)
        size=request.GET.get('size', default=10)
        data['page'] = int(pageNum)
        data['size'] = int(size)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})

    def post(self, request):
        print(request.data)
        import datetime
        request.data['signUpTime'] = datetime.datetime.now()
        request.data['token'] = md5_encoding(request.data['phone'], 'yoga123', request.data['name'])
        print(request.data)
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            data = wrap_data({'token': request.data['token']}, 'user', request.method, True)
        else:
            data = wrap_data({}, 'user', request.method, False)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})


def sub_class(request, token, class_num):
    from django.db import transaction
    from django.db.models import F
    if request.method != 'GET':
        return JsonResponse({}, json_dumps_params={'ensure_ascii': False})
    try:
        user = User.objects.get(token=token)
    except User.DoesNotExist:
        data = wrap_data({}, 'user', request.method, False)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})
    try:
        with transaction.atomic():
            try:
                yoga = YogaClass.objects.get(classNum = class_num, left__gt=0)
                print('Yoga:', yoga)
            except YogaClass.DoesNotExist:
                data = wrap_data({}, 'class', request.method, False)
                return JsonResponse(data, json_dumps_params={'ensure_ascii': False})
            subss = user.subss_user.all()
            for sub in subss:
                if sub.classNum.classNum == yoga.classNum:
                    data = wrap_data({}, 'subs', request.method, False)
                    return JsonResponse(data, json_dumps_params={'ensure_ascii': False})
            print('Before updating')
            yoga.left=F('left')-1
            yoga.save()
            print('After updating')
    except Exception as e:
        print(e)
        return JsonResponse({'msg': 'subscript class failed.', 'success': False}, json_dumps_params={'ensure_ascii': False})
    import datetime
    sub = SubScription(userNum = user, classNum = yoga, subscribeTime = datetime.datetime.now(), overred=False)
    sub.save()
    serializer = UserSerializer(user)
    data = wrap_data(serializer.data, 'subs', request.method, True)
    return JsonResponse(data, json_dumps_params={'ensure_ascii': False})


def user_uncmts(request,token):
    try:
        user = User.objects.get(token = token)
    except User.DoesNotExist:
        data = wrap_data({}, 'user', request.method, False)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})
    uncomments = user.subss_user.filter(commented=False)
    serializers = SubScriptionSerializer(uncomments, many=True)
    data = wrap_data(serializers.data, 'uncomment subscriptions', request.method, True)
    return JsonResponse(data, json_dumps_params={'ensure_ascii': False})


@csrf_exempt
def comment(request, id):
    if request.method != 'POST':
        data = wrap_data({}, '', request.method, False)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})
    try:
        subs = SubScription.objects.get(id=id)
    except SubScription.DoesNotExist:
        data = wrap_data({}, 'sub', request.method, False)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})
    import json
    post = json.loads(request.body)
    print(post)
    subs.classCmt = post.get('lessonCmt')
    subs.teacherCmt = post.get('teacherCmt')
    subs.classCmtLv = post.get('lessonCmtLv')
    subs.teacherCmtLv = post.get('teacherCmtLv')
    print(subs)
    subs.commented = True
    subs.save()
    serializer = SubScriptionSerializer(subs)
    data = wrap_data(serializer.data, 'subs', request.method, True)
    return JsonResponse(data, json_dumps_params={'ensure_ascii': False})



def show_MEDIA_ROOT(request):
    print(settings.MEDIA_ROOT)
    return JsonResponse({'media_root': settings.MEDIA_ROOT}, json_dumps_params={'ensure_ascii': False})