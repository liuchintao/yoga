from django.urls import path

from . import views

urlpatterns = [
    path('show/', views.show_MEDIA_ROOT),
    path('login', views.user_check),
    path('info/<str:token>', views.user_detail),
    path('sub/<str:token>/<str:class_num>', views.sub_class),
    path('uncmt/<str:token>', views.user_uncmts),
    path('cmt/<str:id>', views.comment)
]