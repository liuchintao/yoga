from django.urls import path

from . import views
from apps.user_model.views import user_detail, UsersView

urlpatterns = [
    path('teacher/<int:id>', views.teacher_detail),
    path('teachers', views.TeachersView.as_view()),
    path('teachers/all', views.teacher_all),
    path('lesson/<int:id>', views.lesson_detail),
    path('lessons', views.LessonsView.as_view()),
    path('place/<int:id>', views.place_detail),
    path('places', views.PlacesView.as_view()),
    path('places/all', views.place_all),
    path('classes', views.YogaClassView.as_view()),
    path('classes/<str:type>/<int:id>', views.class_sub),
    path('users', UsersView.as_view()),
    path('admin_models/statistic', views.statistic),
    path('admin_models/baseinfo', views.baseInfo),
]