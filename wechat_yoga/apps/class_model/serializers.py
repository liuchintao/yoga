from rest_framework import serializers

from apps.class_model.models import Lesson, Teacher, Place, YogaClass


class LessonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lesson
        fields = ('lessonNum', 'name', 'difficulty', 'desc')


class TeacherSerializer(serializers.ModelSerializer):
    avatar = serializers.ImageField(max_length=None, allow_empty_file=False, use_url=True)
    class Meta:
        model = Teacher
        fields = ('teacherNum', 'name', 'nickName',
                  'gender', 'age', 'teachingAge', 'desc', 'avatar')


class PlaceSerializer(serializers.ModelSerializer):
    avatar = serializers.ImageField(max_length=None, allow_empty_file=False, use_url=True)
    class Meta:
        model = Place
        fields = ('placeNum', 'name', 'address', 'desc', 'avatar')


class YogaClassSerializer(serializers.ModelSerializer):
    teacher_number = serializers.IntegerField(
        source='teacherNum.teacherNum', read_only=True)
    teacher_name = serializers.CharField(
        source='teacherNum.name', read_only=True)
    lesson_number = serializers.IntegerField(
        source='lessonNum.lessonNum', read_only=True)
    lesson_name = serializers.CharField(
        source='lessonNum.name', read_only=True)
    place_number = serializers.IntegerField(
        source='placeNum.placeNum', read_only=True)
    place_name = serializers.CharField(
        source='placeNum.name', read_only=True)

    class Meta:
        model = YogaClass
        fields = ('classNum', 'teacher_number', 'teacher_name', 'lesson_number',
                  'lesson_name', 'place_number', 'place_name',
                  'desc', 'startTime', 'endTime', 'total', 'left')
