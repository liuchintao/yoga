from django.apps import AppConfig


class ClassModelConfig(AppConfig):
    name = 'class_model'
