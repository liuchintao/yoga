from django.db import models

from django.conf import settings
import os
# Create your models here.
TEACHER_MEDIA_ROOT = settings.MEDIA_ROOT + os.path.sep + 'teacher'
TEACHER_AVATAR_ROOT = TEACHER_MEDIA_ROOT + os.path.sep + 'imgs'

PLACE_MEDIA_ROOT = settings.MEDIA_ROOT + os.path.sep + 'place'
PLACE_AVATAR_ROOT = PLACE_MEDIA_ROOT + os.path.sep + 'imgs'


class Administrator(models.Model):
    username = models.CharField(max_length=16, unique=True)
    password = models.CharField(max_length=16)

    def __str__(self):
        return 'useranme: {}'.format(self.username)


class Lesson(models.Model):
    lessonNum = models.AutoField(primary_key=True)
    name = models.CharField(max_length=16)
    difficulty = models.CharField(max_length=8)
    desc = models.CharField(max_length=512)

    def __str__(self):
        return 'lessonNumber: {}, lessonName: {}'.format(self.lessonNum, self.name)


class Teacher(models.Model):
    teacherNum = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32)
    nickName = models.CharField(max_length=32)
    gender = models.CharField(max_length=8)
    age = models.IntegerField()
    teachingAge = models.IntegerField()
    desc = models.CharField(max_length=512)
    avatar = models.ImageField(upload_to=TEACHER_AVATAR_ROOT, default=None)

    def __str__(self):
        return 'TeacherNumber: {}, TeacherName: {}, Gender: {}, Age: {}'\
            .format(self.teacherNum, self.name, self.gender, self.age)


class Place(models.Model):
    placeNum = models.AutoField(primary_key=True)
    name = models.CharField(max_length=16)
    address = models.CharField(max_length=128)
    desc = models.CharField(max_length=512)
    avatar = models.ImageField(upload_to=PLACE_AVATAR_ROOT, default=None)

    def __str__(self):
        return 'PlaceNumber: {}, PlaceName: {}'.format(self.placeNum, self.name)


class YogaClass(models.Model):
    classNum = models.AutoField(primary_key=True)
    teacherNum = models.ForeignKey('Teacher', related_name="class_teacher", on_delete=models.CASCADE)
    lessonNum = models.ForeignKey('Lesson', on_delete=models.CASCADE)
    placeNum = models.ForeignKey('Place', related_name="class_place", on_delete=models.CASCADE)
    desc = models.CharField(max_length=512)
    startTime = models.DateTimeField()
    endTime = models.DateTimeField()
    total = models.IntegerField(default=0)
    left = models.IntegerField(default=0)

    def __str__(self):
        return 'ClassNumber: {}, TeacherNumber: {}, lessonNumber: {}, PlaceNumber: {}, \
            StartTime: {}, EndTime: {}'\
            .format(self.classNum, self.teacherNum, self.lessonNum, self.placeNum,
                    self.startTime, self.endTime)
