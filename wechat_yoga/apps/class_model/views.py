from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from rest_framework import parsers
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView

from apps.class_model.models import Lesson, Place, Teacher, YogaClass
from apps.user_model.models import User, SubScription
from apps.class_model.serializers import TeacherSerializer, LessonSerializer, \
    PlaceSerializer, YogaClassSerializer
from apps.utils import wrap_data, YogaPageNumberPagination, polish_yoga_class

import datetime
# Create your views here.

def statistic(request):
    teachers = len(Teacher.objects.all())
    places = len(Place.objects.all())
    lessons = len(Lesson.objects.all())
    yoga = len(YogaClass.objects.all())
    user = len(User.objects.all())
    subs = len(SubScription.objects.all())
    data = {
        'teachers': teachers,
        'places': places,
        'lessons': lessons,
        'yoga': yoga,
        'users': user,
        'subs': subs
    }
    data = wrap_data(data, 'all', request.method, True)
    return JsonResponse(data, json_dumps_params={'ensure_ascii': False})

def baseInfo(request):
    teachers = [{'teacherNum': teacher.teacherNum, 'name': teacher.name} for teacher in Teacher.objects.all()]
    places = [{"placeNum": place.placeNum, "name": place.name} for place in Place.objects.all()]
    lessons = [{"lessonNum": lesson.lessonNum, "name": lesson.name} for lesson in Lesson.objects.all()]
    data = {
        'teachers': teachers,
        'places': places,
        'lessons': lessons
    }
    data = wrap_data(data, 'all', request.method, True)
    return JsonResponse(data, json_dumps_params={'ensure_ascii': False})
    

#########################
# Teacher related views #
#########################
@csrf_exempt
def teacher_detail(request, id):
    try:
        teacher = Teacher.objects.get(teacherNum=id)
    except Teacher.DoesNotExist:
        data = wrap_data(None, 'teacher ' + str(id), request.method, False)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})

    if request.method == 'GET':
        serializer = TeacherSerializer(teacher)
        data = wrap_data(serializer.data, 'teacher ' +
                         str(id), request.method, True)

    elif request.method == 'DELETE':
        teacher.delete()
        data = wrap_data({'teacherNum': id}, 'teacher ' +
                         str(id), request.method, True)

    elif request.method == 'PUT':
        jsonData = JSONParser.parse(request)
        serializer = TeacherSerializer(teacher, data=jsonData)
        flag = serializer.is_valid()
        if flag:
            serializer.save()
        data = wrap_data(serializer.data, 'teacher ' +
                         str(id), request.method, flag)

    return JsonResponse(data, json_dumps_params={'ensure_ascii': False})

def teacher_all(request):
    teachers = Teacher.objects.all()
    if request.method == 'GET':
        serializer = TeacherSerializer(teachers, many=True)
        data = wrap_data(serializer.data, 'teacher all', request.method, True)
    return JsonResponse(data, json_dumps_params={'ensure_ascii': False})

class TeachersView(APIView):
    def get(self, request, *args, **kwargs):
        page = YogaPageNumberPagination()
        teachers = Teacher.objects.get_queryset()
        page_teachers = page.paginate_queryset(
            queryset=teachers, request=request, view=self)
        serializer = TeacherSerializer(page_teachers, many=True)
        data = wrap_data(serializer.data, 'teacher list', request.method, True)
        pageNum=request.GET.get('page', default=1)
        size=request.GET.get('size', default=10)
        data['page'] = int(pageNum)
        data['size'] = int(size)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})

    def post(self, request):
        serializer = TeacherSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            data = wrap_data({}, 'teacher', request.method, True)
        else:
            data = wrap_data({}, 'teacher', request.method, False)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})

########################
# Lesson related views #
########################
@csrf_exempt
def lesson_detail(request, id):
    try:
        lesson = Lesson.objects.get(lessonNum=id)
    except Lesson.DoesNotExist:
        data = wrap_data(None, 'lesson ' + str(id), request.method, False)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})

    if request.method == 'GET':
        serializer = LessonSerializer(lesson)
        data = wrap_data(serializer.data, 'lesson ' +
                         str(id), request.method, True)

    elif request.method == 'DELETE':
        lesson.delete()
        data = wrap_data({'lessonNum': id}, 'lesson ' +
                         str(id), request.method, True)

    elif request.method == 'PUT':
        jsonData = JSONParser.parse(request)
        serializer = LessonSerializer(lesson, data=jsonData)
        flag = serializer.is_valid()
        if flag:
            serializer.save()
        data = wrap_data(serializer.data, 'lesson ' +
                         str(id), request.method, flag)

    return JsonResponse(data, json_dumps_params={'ensure_ascii': False})


class LessonsView(APIView):
    def get(self, request, *args, **kwargs):
        page = YogaPageNumberPagination()
        lessons = Lesson.objects.get_queryset()
        page_lessons = page.paginate_queryset(
            queryset=lessons, request=request, view=self)
        serializer = LessonSerializer(page_lessons, many=True)
        data = wrap_data(serializer.data, 'lesson list', request.method, True)
        pageNum=request.GET.get('page', default=1)
        size=request.GET.get('size', default=10)
        data['page'] = int(pageNum)
        data['size'] = int(size)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})

    def post(self, request):
        serializer = LessonSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            data = wrap_data(dict(serializer.validated_data),
                             'lesson', request.method)
        else:
            data = wrap_data(jsonData, 'lesson', request.method, False)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})

#######################
# Place related views #
#######################
@csrf_exempt
def place_detail(request, id):
    try:
        place = Place.objects.get(plcaeNum=id)
    except Place.DoesNotExist:
        data = wrap_data(None, 'place ' + str(id), request.method, False)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})

    if request.method == 'GET':
        serializer = PlaceSerializer(place)
        data = wrap_data(serializer.data, 'place ' + str(id))
    elif request.method == 'DELETE':
        place.delete()
        data = wrap_data({'placeNum': id}, 'place ' + str(id), request.method)
    elif request.method == 'PUT':
        jsonData = JSONParser.parse(request)
        serializer = PlaceSerializer(place, data=jsonData)
        flag = serializer.is_valid()
        if flag:
            serializer.save()
        data = wrap_data(serializer.data, 'place ' +
                         str(id), request.method, flag)
    return JsonResponse(data, json_dumps_params={'ensure_ascii': False})

def place_all(request):
    places = Place.objects.all()
    if request.method == 'GET':
        place_serializer = PlaceSerializer(places, many=True)
        data = wrap_data(place_serializer.data, 'place all', request.method, True)
    return JsonResponse(data, json_dumps_params={'ensure_ascii': False})


class PlacesView(APIView):
    parser_classes = (parsers.FormParser, parsers.MultiPartParser,
                      parsers.JSONParser, parsers.FileUploadParser,)

    def get(self, request, *args, **kwargs):
        page = YogaPageNumberPagination()
        places = Place.objects.get_queryset()
        page_places = page.paginate_queryset(
            queryset=places, request=request, view=self)
        serializer = PlaceSerializer(page_places, many=True)
        data = wrap_data(serializer.data, 'place list', request.method, True)
        pageNum=request.GET.get('page', default=1)
        size=request.GET.get('size', default=10)
        data['page'] = int(pageNum)
        data['size'] = int(size)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})

    def post(self, request):
        serializer = PlaceSerializer(data=request.data)
        print(request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            data = wrap_data({}, 'place', request.method, True)
        else:
            data = wrap_data({}, 'place', request.method, False)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})

###########################
# YogaClass related views #
###########################
class YogaClassView(APIView):
    parser_classes = (parsers.FormParser, parsers.MultiPartParser,
                      parsers.JSONParser, parsers.FileUploadParser,)

    def get(self, request, *args, **kwargs):
        page = YogaPageNumberPagination()
        now = datetime.datetime.now()
        start = now - datetime.timedelta(hours=23, minutes=59, seconds=59)
        yoga_classes = YogaClass.objects.filter(startTime__gte=start)
        page_classes = page.paginate_queryset(
            queryset=yoga_classes, request=request, view=self)
        serializer = YogaClassSerializer(page_classes, many=True)
        data = wrap_data(serializer.data, 'class list', request.method, True)
        pageNum=request.GET.get('page', default=1)
        size=request.GET.get('size', default=10)
        data['page'] = int(pageNum)
        data['size'] = int(size)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})
    
    def post(self, request):
        lessonId = request.data.get('lesson_number')
        teacherId = request.data.get('teacher_number')
        placeId = request.data.get('place_number')
        try:
            lesson = Lesson.objects.get(lessonNum=lessonId)
        except Lesson.DoesNotExist:
            data = wrap_data(None, 'lesson ' + str(lessonId), request.method, False)
            return JsonResponse(data, json_dumps_params={'ensure_ascii': False})
        try:
            teacher = Teacher.objects.get(teacherNum=teacherId)
        except Teacher.DoesNotExist:
            data = wrap_data(None, 'teacher ' + str(lessonId), request.method, False)
            return JsonResponse(data, json_dumps_params={'ensure_ascii': False})
        try:
            place = Place.objects.get(placeNum=placeId)
        except Place.DoesNotExist:
            data = wrap_data(None, 'place ' + str(lessonId), request.method, False)
            return JsonResponse(data, json_dumps_params={'ensure_ascii': False})
        startTime = datetime.datetime.strptime(request.data.get('startTime'), '%Y-%m-%d %H:%M:%S')
        endTime = datetime.datetime.strptime(request.data.get('endTime'), '%Y-%m-%d %H:%M:%S')
        total = request.data.get('total')
        yogaClass = YogaClass(teacherNum=teacher, placeNum=place, lessonNum=lesson, startTime=startTime, endTime=endTime, total=total, left=total)
        yogaClass.save()
        serializer = YogaClassSerializer(yogaClass)
        data = wrap_data(serializer.data, 'place ' + str(lessonId), request.method, False)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})

def class_sub(request, type, id):
    now = datetime.datetime.now()
    start = now - datetime.timedelta(hours=23, minutes=59, seconds=59)
    end = now + datetime.timedelta(hours=167, minutes=59, seconds=59)
    if type == 'teacher':
        try:
            teacher = Teacher.objects.get(teacherNum=id)
        except Teacher.DoesNotExist:
            yogas = YogaClass.objects.filter(startTime__gte=start, startTime__lte=end)
        yogas = teacher.class_teacher.filter(startTime__gte=start, startTime__lte=end)
    elif type == 'place':
        try:
            place = Place.objects.get(placeNum=id)
        except Place.DoesNotExist:
            yogas = YogaClass.objects.filter(startTime__gte=start, startTime__lte=end)
        yogas = place.class_place.filter(startTime__gte=start, startTime__lte=end)
    else:
        yogas = YogaClass.objects.filter(startTime__gte=start, startTime__lte=end)
    data = wrap_data(polish_yoga_class(yogas), 'teacher ' + str(id), request.method, False)
    return JsonResponse(data, json_dumps_params={'ensure_ascii': False})