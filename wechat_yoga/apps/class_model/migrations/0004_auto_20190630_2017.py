# Generated by Django 2.2.2 on 2019-06-30 20:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('class_model', '0003_auto_20190619_1817'),
    ]

    operations = [
        migrations.AddField(
            model_name='place',
            name='avatar',
            field=models.ImageField(default=None, upload_to='D:\\DevelopeEnv\\WeChats\\YogaBusiness\\wechat_yoga\\media\\teacher\\imgs'),
        ),
        migrations.AddField(
            model_name='teacher',
            name='avatar',
            field=models.ImageField(default=None, upload_to='D:\\DevelopeEnv\\WeChats\\YogaBusiness\\wechat_yoga\\media\\teacher\\imgs'),
        ),
    ]
