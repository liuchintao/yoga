// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import router from './router'
import Vuex from 'vuex'
import VueResource from 'vue-resource'
Vue.use(Element)
Vue.use(Vuex)
Vue.use(VueResource)

Vue.config.productionTip = false
const store = new Vuex.Store({
  state: {
    // serverHost: 'http://localhost:8000',
    // serverHost: 'http://47.101.137.127',
    serverHost: 'http://49.234.26.213/',
    adminApi: '/api/class',
    classApi: '/api/user',
    placesCount: 0,
    teachersCount: 0,
    lessonsCount: 0,
    teachers: {},
    token: ''
  },
  mutations: {
    saveServerHost (state, serverHost) {
      state.serverHost = serverHost
    },
    savePlacesCount (state, placesCount) {
      state.placesCount = placesCount
    },
    saveTeachersCount (state, teachersCount) {
      state.teachersCount = teachersCount
    },
    saveLessonsCount (state, lessonsCount) {
      state.lessonsCount = lessonsCount
    },
    saveToken (state, token) {
      state.token = token
    }
  }
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
  router,
  store
})
