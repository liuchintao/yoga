import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [{
    path: '/',
    redirect: '/login'
  },
  {
    path: '/join',
    component: resolve => require(['@/components/signUp.vue'], resolve),
    name: 'user_join'
  }, {
    path: '/login',
    component: resolve => require(['@/components/login.vue'], resolve),
    name: 'login'
  }, {
    path: '/admin',
    redirect: '/admin/user'
  }, {
    path: '/admin',
    component: resolve => require(['@/components/admin_home.vue'], resolve),
    name: 'admin_home',
    children: [
      {
        path: 'place',
        component: resolve => require(['@/components/admin/place.vue'], resolve),
        name: 'admin_place'
      },
      {
        path: 'teacher',
        component: resolve => require(['@/components/admin/teacher.vue'], resolve),
        name: 'admin_teacher'
      },
      {
        path: 'lesson',
        component: resolve => require(['@/components/admin/lesson.vue'], resolve),
        name: 'admin_lesson'
      },
      {
        path: 'class',
        component: resolve => require(['@/components/admin/yoga_class.vue'], resolve),
        name: 'admin_class'
      },
      {
        path: 'user',
        component: resolve => require(['@/components/admin/user.vue'], resolve),
        name: 'admin_user'
      }
    ]
  }, {
    path: '/yoga',
    redirect: '/yoga/overview'
  }, {
    path: '/yoga',
    component: resolve => require(['@/components/yoga_home.vue'], resolve),
    name: 'yoga',
    meta: {
      title: '源慧瑜珈'
    },
    children: [
      {
        path: 'overview',
        component: resolve => require(['@/components/class/overview.vue'], resolve),
        name: 'class_overview'
      }, {
        path: 'center',
        component: resolve => require(['@/components/class/center.vue'], resolve),
        name: 'user_center'
      }, {
        path: 'sub',
        component: resolve => require(['@/components/class/sub.vue'], resolve),
        name: 'user_subscription'
      }, {
        path: 'comment',
        component: resolve => require(['@/components/class/comment.vue'], resolve),
        name: 'user_comment'
      }
    ]
  }
  ]
})
