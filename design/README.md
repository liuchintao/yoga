- [Yoga Management System Design](#yoga-management-system-design)
  - [Model Design](#model-design)
  - [RESTFul API Design](#restful-api-design)
    - [Models' API](#models-api)
      - [Teacher](#teacher)
      - [Lesion](#lesion)
      - [Place](#place)
      - [YogaClass](#yogaclass)
      - [User](#user)
      - [Subscription](#subscription)

# Yoga Management System Design

## Model Design

| 模块  | 类           | 属性                                                                                                                                                                           | 主键 | 说明                                                                                                                                                                                       |
| ----- | ------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ---- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Admin | Admin        | **id**, username, password                                                                                                                                                     | id   | 用于管理系统内所有数据                                                                                                                                                                     |
| Admin | Teacher      | **id**, teacherNum, name, nickname, gender, age, teachingAge, desc                                                                                                             | id   | **id**由系统分配，teacherNum自增且唯一，教师姓名可以重复，但是绰号需要唯一                                                                                                                                                     |
| Admin | Lesion       | **id**, lesionNum, name, difficulty, desc                                                                                                                                      | id   | **id**由系统分配，lesionNum自增且唯一                                                                                                                                                      |
| Admin | Place        | **id**, placeNum, name, address, desc                                                                                                                                          | id   | **id**由系统分配，placeNum自增且唯一                                                                                                                                                       |
| Admin | YogaClass    | **id**, classNum, *teacherNum, lesionNum, placeNum*, teacherName, lessionName, placeName, desc, classDate, startTime, endTime, isRepeatable, period(week/month/custom), repeatTimes, isSubstitute(default is False) | id   | classNum自增且唯一，每一次课程都依赖“教师”、“课程”和“上课地点”，课程是否按照一定日期规则重复现在先不做，视为自己之后的兴趣小项目开发，不计入交付产品中；是否替课逻辑视开发进度决定是否交付 |
| User  | User         | **id**, userNum, name, age, gender, phone, signUpTime...                                                                                                                                | id   | **id**通过hash等方法获得，具体字段放在之后和微信接口对接后决定                                                                                                                             |
| User  | Subscription | **id**, *userNum, classNum*, isOverred, classCmtLv, teacherCmtLv, classCmt, teacherCmt, subscribeTime, concealed                                                                                         | id   | 记录用户预约课程以及用户对已经过期的课程、老师的评价、星级，用户姓名可以重复                                                                                                                                 |

> 1. **YogaClass**课程是否按照一定日期规则重复现在先不做，视为自己之后的兴趣小项目开发，不计入交付产品中；是否替课逻辑视开发进度决定是否交付 

## RESTFul API Design

### Models' API

#### Teacher

1. ***POST*** api/teacher
   
   ```JSON
   {
       success: true,
       code: 100,
       msg: 'add teacher successfully',
   },{
       success: false,
       code: 110,
       msg: 'add teacher failed',
   }
   ```

2. ***DELETE*** api/teacher/\<teacherNum>
   
   ```JSON
   {
       success: true,
       code: 200,
       msg: 'delete teacher successfully',
   },{
       success: false,
       code: 210,
       msg: 'delete teacher failed',
   }
   ```

3. ***PUT*** api/teacher 
   
   ```JSON
   {
       success: true,
       code: 300,
       msg: 'update teacher successfully',
   },{
       success: false,
       code: 310,
       msg: 'update teacher failed',
   }
   ```

4. ***GET*** api/teacher/\<nickName>
   
   ```JSON
   {
       success: true,
       code: 400,
       msg: 'select teacher successfully',
       data:{
           teacherNum: 1,
           name: 'Tom',
           nickName: 'Tom Cat',
           age: 20,
           teachingAge: 1
       }
   },{
       success: false,
       code: 410,
       msg: 'select teacher failed',
       data: null,
   }
   ```
   
5. ***GET*** api/teachers?per_page=\<number>&page=\<page_number>
   
   ```JSON
   {
       success: true,
       code: 400,
       msg: 'select teachers successfully',
       data:[{
           teacherNum: 1,
           name: 'Tom',
           nickName: 'Tom Cat',
           age: 20,
           teachingAge: 1
       },],
       page: 0,
       per_page: 1,
   },{
       success: false,
       code: 410,
       msg: 'select teachers failed',
       data: null,
   }
   ```

#### Lesion

1. ***POST*** api/lesion
   
   ```JSON
   {
       success: true,
       code: 100,
       msg: 'add lesion successfully',
   },{
       success: false,
       code: 110,
       msg: 'add lesion failed',
   }
   ```

2. ***DELETE*** api/lesion/\<lesionNum>
   
   ```JSON
   {
       success: true,
       code: 200,
       msg: 'delete lesion successfully',
   },{
       success: false,
       code: 210,
       msg: 'delete lesion failed',
   }
   ```

3. ***PUT*** api/lesion 
   
   ```JSON
   {
       success: true,
       code: 300,
       msg: 'update lesion successfully',
   },{
       success: false,
       code: 310,
       msg: 'update lesion failed',
   }
   ```

4. ***GET*** api/lesion/\<name>
   
   ```JSON
   {
       success: true,
       code: 400,
       msg: 'select lesion successfully',
       data:{
           lession: 1,
       }
   },{
       success: false,
       code: 410,
       msg: 'select lesion failed',
       data: null,
   }
   ```

5. ***GET*** api/lesions?per_page=\<number>&page=\<page_number>
   
   ```JSON
   {
       success: true,
       code: 400,
       msg: 'select lesions successfully',
       data:[{
           lesionNum: 1,
       },],
       page: 0,
       per_page: 1,
   },{
       success: false,
       code: 410,
       msg: 'select lesions failed',
       data: null,
   }
   ```

#### Place

1. ***POST*** api/place
   
   ```JSON
   {
       success: true,
       code: 100,
       msg: 'add place successfully',
   },{
       success: false,
       code: 110,
       msg: 'add place failed',
   }
   ```

2. ***DELETE*** api/place/\<placeNum>
  
   ```JSON
   {
       success: true,
       code: 200,
       msg: 'delete place successfully',
   },{
       success: false,
       code: 210,
       msg: 'delete place failed',
   }
   ```

3. ***PUT*** api/place 
   
   ```JSON
   {
       success: true,
       code: 300,
       msg: 'update place successfully',
   },{
       success: false,
       code: 310,
       msg: 'update place failed',
   }
   ```

4. ***GET*** api/place/\<name>
  
   ```JSON
   {
       success: true,
       code: 400,
       msg: 'select place successfully',
       data:{
           placeNum: 1,
           name: '养生馆',
           address: '武汉市武昌区某条街的某号的某栋楼',
           desc: '有节操，有涵养',
       }
   },{
       success: false,
       code: 410,
       msg: 'select place failed',
       data: null,
   }
   ```

5. ***GET*** api/places?per_page=\<number>&page=\<page_number>
  
   ```JSON
   {
       success: true,
       code: 400,
       msg: 'select places successfully',
       data:[{
           placeNum: 1,
           name: '养生馆',
           address: '武汉市武昌区某条街的某号的某栋楼',
           desc: '有节操，有涵养',
       },],
       page: 0,
       per_page: 1,
   },{
       success: false,
       code: 410,
       msg: 'select places failed',
       data: null,
   }
   ```

#### YogaClass

1. ***POST*** api/yoga
  
   ```JSON
   {
       success: true,
       code: 100,
       msg: 'add yoga successfully',
   },{
       success: false,
       code: 110,
       msg: 'add yoga failed',
   }
   ```

2. ***DELETE*** api/yoga/\<classNum>
   
   ```JSON
   {
       success: true,
       code: 200,
       msg: 'delete yoga successfully',
   },{
       success: false,
       code: 210,
       msg: 'delete yoga failed',
   }
   ```

3. ***PUT*** api/yoga 
   
   ```JSON
   {
       success: true,
       code: 300,
       msg: 'update yoga successfully',
   },{
       success: false,
       code: 310,
       msg: 'update yoga failed',
   }
   ```

4. ***GET*** api/yogas?per_page=\<number>&page=\<page_number>&before=\<timestamp>&after=\<timestamp>&pName=\<placeName>&tName=\<teacherName>&lDfclt=\<difficulty>&lName=\<lessionName>
   
   ```JSON
   {
       success: true,
       code: 400,
       msg: 'select yogas successfully',
       data:[{
           classNum: 1,
           teacherName: 'Tom',
           placeName: '养生馆',
           address: '武汉市武昌区某条街的某号的某栋楼',
           classDate: 'yyyy-mm-dd',
           startTime: 'hh-MM-ss',
           endTime: 'hh-MM-ss',
           difficulty: 'easy'
       },],
       page: 0,
       per_page: 1,
   },{
       success: false,
       code: 410,
       msg: 'select yogas failed',
       data: null,
   }
   ```

#### User

1. ***POST*** api/user
   
   ```JSON
   {
       success: true,
       code: 100,
       msg: 'add user successfully',
   },{
       success: false,
       code: 110,
       msg: 'add user failed',
   }
   ```

2. ***DELETE*** api/user/\<userNum>
  
   ```JSON
   {
       success: true,
       code: 200,
       msg: 'delete user successfully',
   },{
       success: false,
       code: 210,
       msg: 'delete user failed',
   }
   ```

3. ***PUT*** api/user 
   
   ```JSON
   {
       success: true,
       code: 300,
       msg: 'update user successfully',
   },{
       success: false,
       code: 310,
       msg: 'update user failed',
   }
   ```

4. ***GET*** api/users/\<name>
   
   ```JSON
   {
       success: true,
       code: 400,
       msg: 'select users successfully',
       data:[{
           userNum: 1,
           name: 'Fresh Fish',
       }]
   },{
       success: false,
       code: 410,
       msg: 'select users failed',
       data: null,
   }
   ```

5. ***GET*** api/users?per_page=\<number>&page=\<page_number>&before=\<signUpTimeStamp>&after=\<signUpTimeStamp>
   
   ```JSON
   {
       success: true,
       code: 400,
       msg: 'select users successfully',
       data:[{
           userNum: 1,
           name: 'Fresh Fish',
       },],
       page: 0,
       per_page: 1,
   },{
       success: false,
       code: 410,
       msg: 'select users failed',
       data: null,
   }
   ```

6. ***GET*** api/user?token=\<weChatToken>

    ```JSON
    {
        success: true,
       code: 400,
       msg: 'select user successfully',
       data:{
           userNum: 1,
           name: 'Fresh Fish',
       },
       page: 0,
       per_page: 1,
    },{
       success: false,
       code: 410,
       msg: 'select user failed',
       data: null,
   }
    ```

#### Subscription

1. ***POST*** api/subscription
2. ***PUT*** api/subscription/comments

只能提交一次，当teacherCmt, teacherCmtLv, classCmt, classCmtLv中至少一项存在内容，就认为用户已经评论

3. ***GET*** api/subscriptions/userNum?before=\<subscribeTimestamp>&after=\<subscribeTimestamp>&asc=\<true or false>