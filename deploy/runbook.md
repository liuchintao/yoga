- [Yoga系统使用手册](#Yoga%E7%B3%BB%E7%BB%9F%E4%BD%BF%E7%94%A8%E6%89%8B%E5%86%8C)
  - [Admin部分](#Admin%E9%83%A8%E5%88%86)
  - [User部分](#User%E9%83%A8%E5%88%86)
  - [后期处理和收尾](#%E5%90%8E%E6%9C%9F%E5%A4%84%E7%90%86%E5%92%8C%E6%94%B6%E5%B0%BE)

# Yoga系统使用手册

网站分为了两个部分，分别是管理部分和用户部分，管理部分负责查看、添加课程、场所、课时、教练、用户信息；用户部分主要用来展示给用户看。

## Admin部分

1. 用**电脑**进入[管理端网站](http://49.234.26.213/admin/)（手机进入体验不好）
2. **查看并添加场馆**

![场馆管理](https://gitlab.com/ljt2016/public-img/raw/0b739a25e3a80278b2571198038a621eeaebc08e/Wechat/yoga/admin.png)

左侧为导航栏，高亮浅蓝色文字说明当前所管理的对象是场馆、课程还是教练。此时我们管理的是瑜伽场馆信息。

点击最右边的**蓝色小蜡笔图标**可以添加场馆信息。如下图所示：

![场馆添加](https://gitlab.com/ljt2016/public-img/raw/master/Wechat/yoga/admin_place.png)

**注意**  
需要注意的是，**场馆照片是必填项**，没有照片会导致**添加失败**或用户端展示**效果不好**。

3. **查看并添加教练**

与场馆管理类似，教练信息需要管理员或经理手动添加。

![教练管理](https://gitlab.com/ljt2016/public-img/raw/master/Wechat/yoga/admin_teacher.png)

**注意**  
需要注意的是，**教练照片是必填项**，没有照片会导致**添加失败**或用户端展示**效果不好**。

4. **查看并添加课程**

这里需要说明的是，系统由**课程**和**课时**两个概念，**课程**描述了某一门瑜伽课的名称、难度和基本信息等。**课时**描述的是这一门瑜伽课在从几点几时开始几点几时结束，由哪位老师在哪个场馆上课。

![课程管理](https://gitlab.com/ljt2016/public-img/raw/master/Wechat/yoga/admin_lesson.png)

5. **查看并添加课时**

课时管理需要由管理员手动选择上课时间、地点、教练等内容。

![课时管理](https://gitlab.com/ljt2016/public-img/raw/master/Wechat/yoga/admin_class.png)

6. **查看并管理学员**

在用户管理界面可以查看已经注册了的学院的基本信息，同样也可以通过右侧小蜡笔图标由管理员添加新用户。

![用户管理](https://gitlab.com/ljt2016/public-img/raw/master/Wechat/yoga/admin_user.png)

## User部分

1. **登陆注册**

用户部分主要供用户使用，用户通过微信端进入系统后需要手动登陆，登陆信息只需要用户注册时填写的用户名和注册时填写的手机号。

![用户登录](https://gitlab.com/ljt2016/public-img/raw/master/Wechat/yoga/yoga_login.png)

新用户则需要点击**注册**按钮进行注册，注册界面中强制了用户需要上传用户头像。

![用户注册](https://gitlab.com/ljt2016/public-img/raw/master/Wechat/yoga/yoga_signup.png)

2. **用户主页**

在用户主页，用户可以浏览管理员添加的场馆和教练信息。

![主页](https://gitlab.com/ljt2016/public-img/raw/master/Wechat/yoga/yoga_ov.png)

用户通过点击场馆图标或教练图标，即可进入相关的课程预约界面

3. **课程预约**

用户通过点击教练、场馆头像进入预约界面时，预约界面只会显示与该场馆、教练有关的**未来七天内**的课程信息。

如果用户通过上方导航栏进入预约界面时，预约界面会显示未来七天内所有的课程信息。

![课程预约](https://gitlab.com/ljt2016/public-img/raw/master/Wechat/yoga/yoga_sub.png)

4. 用户中心

用户中心主要负责显示用户基本信息和预约课程信息，会向用户展示已开始和未开始的课程信息。但是不会展示已经结束的课程信息。

![用户中心](https://gitlab.com/ljt2016/public-img/raw/master/Wechat/yoga/yoga_ct.png)

5. 课程评价

用户通过上方导航栏进入课程评价界面，评价界面会给出所有用户已经预约了的、且未评价课程的列表，用户点击评价按钮即可给所选课程和教练打分和评价。

![课程评价](https://gitlab.com/ljt2016/public-img/raw/master/Wechat/yoga/yoga_cmt.png)

## 后期处理和收尾

登陆微信公众号平台，在左边侧边栏的最下边选择“**接口权限**”，按步骤完成最后的收尾工作。

![登陆公众号](https://gitlab.com/ljt2016/public-img/raw/master/Wechat/yoga/fix_1.png)

![网页授权](https://gitlab.com/ljt2016/public-img/raw/master/Wechat/yoga/fix_2.png)

![功能设置](https://gitlab.com/ljt2016/public-img/raw/master/Wechat/yoga/fix_3.png)

![添加域名](https://gitlab.com/ljt2016/public-img/raw/master/Wechat/yoga/fix_4.png)

![添加域名](https://gitlab.com/ljt2016/public-img/raw/master/Wechat/yoga/fix_5.png)

**注意**
需要添加的域名有：www.yuanhuiyoga.com www.yuanhuiyoga.com/mp www.yuanhuiyoga.com/yoga